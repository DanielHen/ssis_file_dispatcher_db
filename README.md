***Created by: Daniel Dabrowski dabrowski.daniel@interia.pl***

***Description:***
SSIS_Dispatcher_db has been created in order to demonstrate how to dispatches
files to different folders(based on file name). File name list is based on ms sql table.
This approach simplify adding new file name to ssis package. There is no need to re-deploy package.   
Feel free to use,modify at your will.It has been tested  on Microsoft SQL Serve 2016.

***How it works:***
SSIS pacgage  dispatches files to respective folders based on file name. 
Please bear in mind that connection is to Nortwind databse(common training database). Before 
run this package make sure that proper connection is established.

***Prerequisitions:*** 
Create a proper database where all your file names are stored. Package extract table rows 
to object variable.

--MS sql server
create table dispatcher
(id int primary key identity(1,1),
file_name varchar(255)
)

--Insert exemplary file names
 insert into dispatcher
  (report_name)
  values
  ('WKRM'),
  ('LARK'),
  ('MAT'),
  ('HOK')



Before initializing this package a proper folder scheme need to be createdas as well.
In order to do so please clone Python script(https://gitlab.com/DanielHen/flfo_create.git). Run Python script in order to obtaion a proper
file/folder scheme like below.


***INITIAL SCHEME***
<pre>
C:.
|
|
+---SSIS_Dispatcher
    |
    |
    +---FROM
    |   +---HOK20191201.txt
    |   +---HOK20191202.txt
    |   ....
    |   +---LARK20191201.txt
    |   +---LARK20191202.txt
    |    .... 
    |   +---MAT20191201.txt
    |   +---MAT20191202.txt
    |    .... 
    |   +---WKRM20191201.txt
    |   +---WKRM20191202.txt
    |    .... 
    |
    +---TO
    |   +---HOK
    |   +---LARK
    |   +---MAT
    |   +---WKRM
</pre>

***Explanation:***
In folder "FROM" there are all files. File name consist of report name like 'LARK' concatenate with
raport date e.g. LARK20200219.
Folder TO consistens of subfloders within. Folders name within are WKRM, LARK, MAT, HOK.
Package dispatches files based on report name to respective subfolders of TO folder.
The outcome folder/file  scheme will  look like this one below.

***RESULT SCHEME***

<pre>
C:.
|
|
+---SSIS_Dispatcher
    |
    |
    +---FROM
    |   +---HOK20191201.txt
    |   +---HOK20191202.txt
    |   ....
    |   +---LARK20191201.txt
    |   +---LARK20191202.txt
    |   .... 
    |   +---MAT20191201.txt
    |   +---MAT20191202.txt
    |    .... 
    |   +---WKRM20191201.txt
    |   +---WKRM20191202.txt
    |    .... 
    |
    +---TO
    |   +---HOK
    |   |    +---HOK20191201.txt
    |   |    +---HOK20191202.txt
    |   |    ....
    |   +---LARK
    |   |    +---LARK20191201.txt
    |   |    +---LARK20191202.txt 
    |   |    ....
    |   +---MAT
    |   |    +---MAT20191201.txt
    |   |    +---MAT20191202.txt
    |   |    .... 
    |   +---WKRM
    |   |   +---WKRM20191201.txt
    |   |   +---WKRM20191202.txt
    |   |   .... 
</pre>















Created by: Daniel Dabrowski dabrowski.daniel@interia.pl
Description:
Dispatcher_db ssis solution has been created in order to demonstrate how to dispatches
files to different folders(based on file name).In this project file names  which has to be dispatched are based 
on database table records. That approach simplify adding new files to ssis package. There is no need to re-deploy package after new file
name is add to dispatching list. Feel free to use,modify this SSIS package.
It has been tested  on Microsoft SQL Serve 2016.
How it works:
SSIS pacgage  dispatches files to respective folders based on file name.
Prequsition: Before initializing this package a proper folder scheme need to be created.
In order to do so please clone Python script(https://gitlab.com/DanielHen/flfo_create.git). Run Python script in order to obtaion a proper
file/folder scheme like below. This package is connected to Nortwind exemplary database
,feel  free to change this connection if needed. Please use script below in order to create a proper table which stores dispatcher file list. 

