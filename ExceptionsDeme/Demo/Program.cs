﻿using Exceptions_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        static void Main(string[] args)
        {
            DemoCode demo = new DemoCode();

            try
            {
                int result = demo.GrandparentMethod(5);
                Console.WriteLine($"The value at given position is { result }");
            }
            catch (Exception ex )
            {

                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
            }

           

            Console.ReadLine();

        }
    }
}
