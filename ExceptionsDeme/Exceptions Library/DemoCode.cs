﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions_Library
{
    public class DemoCode
    {
        public int GrandparentMethod(int position)
        {
            int output = 0;

            Console.WriteLine("Open Database Connection");

            try
            {
                output = ParentMethod(position);
            }
            catch (Exception ex)
            {
                //Do some logging
                throw;
                //Hey I'm ignoring the fact that we had caught this
                //exception I'm gonna throw it back and let the next item up
                //the stack catch it. Onzczacz to że try catch z poziomu wyżej 
                //wyłapie to  exception i poda pełną informację, ale przed tym
                //uruchomi finally code.
            }
            finally
            {
                Console.WriteLine("Close Database Connection");
            }

            Console.WriteLine("Close Database Connection");

            return output;

        }


        public int ParentMethod(int position)
        {
            return GetNumber(position);
        }

        public int GetNumber(int position)
        {

            int output = 0;

            //try
            //{
                int[] numbers = new int[] { 1, 4, 7, 2 };
                return numbers[position];
            //}
            //catch (Exception ex)
            //{

            //    Console.WriteLine(ex.Message);
            //    Console.WriteLine(ex.StackTrace);
            //}

            return output;

        }
    }
}
